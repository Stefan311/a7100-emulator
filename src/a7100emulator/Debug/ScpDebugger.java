package a7100emulator.Debug;

import java.io.FileWriter;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import a7100emulator.Tools.ConfigurationManager;
import a7100emulator.components.system.MMS16Bus;

/**
 * Klasse um Betriebssystemaufrufe auf SCP zu loggen
 * @author Stefan Berndt
 *
 */
public class ScpDebugger 
{
    /**
     * Logger Instanz
     */
    private static final Logger LOG = Logger.getLogger(ScpDebugger.class.getName());
    
    /**
     * Globaler Debugger
     */
    private static final Debugger globalDebugger = Debugger.getGlobalInstance();
    
    /**
     * MMS16 - Systembus des A7100
     */
    private static final MMS16Bus mms16 = MMS16Bus.getInstance();

    /**
     * Instanz
     */
    private static ScpDebugger instance;
    
    /**
     * logfile
     */
    private FileWriter logFile = null;

    /**
     * Funktionsnummer beim letzten SCP-Aufruf 
     */
    private int SCPFunction = 0;
    
    /**
     * DX-Register beim letzen SCP-Aufruf
     */
    private int SCPParamDX = 0;

    /**
     * DS-Register beim letzen SCP-Aufruf
     */
    private int SCPParamDS = 0;

    /**
     * Offset-Adresse des DMA-Buffers
     */
    private int SCPDmaDX = 0;
    
    /**
     * Segment-Adresse des DMA-Buffers 
     */
    private int SCPDmaDS = 0;
    
    /**
     * Gibt die statische Instanz des ScpDebugger zurück
     *
     * @return Instanz
     */
    public static ScpDebugger getInstance() 
    {
        if (instance == null) 
        {
            instance = new ScpDebugger();
        }
        return instance;
    }

    /**
     * Der Konstruktor
     */
    public ScpDebugger()
    {
    	if (ConfigurationManager.getInstance().readInteger("Debugger", "SCP", 0) > 0)
    	{
    		setDebug(true);
    	}
    }
    
    /**
     * Gibt an ob der Debugger aktuell aktiv ist
     *
     * @return true - Debugger aktiv , false - Debugger inaktiv
     */
    public boolean isDebug() {
        return logFile != null;
    }

    /**
     * Startet den Debugger oder hält ihn an
     *
     * @param debug true - wenn Debugger gestartet werden soll , false - sonst
     */
    public void setDebug(boolean debug) 
    {
        if (logFile == null && debug) 
        {
            String directory = ConfigurationManager.getInstance().readString("directories", "debug", "./debug/");
            try 
            {
                logFile = new FileWriter(directory + "scp.log");
            } 
            catch (IOException ex) 
            {
                LOG.log(Level.WARNING, "Fehler beim Erzeugen der SCP-Debug-Ausgabe!", ex);
            }
        }
        if (logFile != null && !debug) 
        {
        	try 
        	{
				logFile.flush();
	        	logFile.close();
			} 
        	catch (IOException ex) 
        	{
                LOG.log(Level.WARNING, "Fehler beim Schließen der SCP-Debug-Ausgabe!", ex);
			}
        	logFile = null;
        }
    }

    /**
     * Add Line to Logfile
     * @param text der Text
     */
    public void addLine(String text) 
    {
        if (globalDebugger.isDebug()) 
        {
            globalDebugger.addComment("SCP: " + text);
        } 
    	if (logFile == null) return;
        try 
        {
            logFile.write(text + "\n");
            logFile.flush();
        } 
        catch (IOException ex) 
        {
            LOG.log(Level.WARNING, "Fehler beim Schreiben der SCP-Debug-Ausgabe!", ex);
        }    	
    }

    /**
     * Schreibt 128 Bytes aus Speicher in den Log
     * @param DS Segment
     * @param DX Offset
     */
    private void LogDmaBuffer(int DS, int DX)
    {
    	String s = "DMA Data={";
    	for (int a=0;a<128;a++)
    	{
    		s += String.format("%02X ", mms16.readMemoryByte((DS << 4) + DX + a));
    	}
    	s += "}";
    	addLine(s);
    }
    
    /**
     * Schreibt den Inhalt eines FCB in den Log
     * @param DS Segment
     * @param DX Offset
     */
    private void LogFCB(int DS, int DX)
    {
    	String s = "FCB Drive=" + mms16.readMemoryByte((DS << 4) + DX)+" Name=\"";
    	for (int a=0; a<8; a++) s += (char)mms16.readMemoryByte((DS << 4) + DX + 1 + a);
    	s += ".";
    	for (int a=0; a<3; a++) s += (char)(mms16.readMemoryByte((DS << 4) + DX + 9 + a) & 0x7f);
    	s += "\" EX=" + mms16.readMemoryByte((DS << 4) + DX + 12);
    	s += " RC=" + mms16.readMemoryByte((DS << 4) + DX + 15);
    	s += " CR=" + mms16.readMemoryByte((DS << 4) + DX + 32);
    	s += " R0=" + mms16.readMemoryByte((DS << 4) + DX + 33);
    	s += " R1=" + mms16.readMemoryByte((DS << 4) + DX + 34);
    	s += " R2=" + mms16.readMemoryByte((DS << 4) + DX + 35);
    	addLine(s);
    }

    /**
     * Schreibt einen MCB in den Log
     * @param DS Segment
     * @param DX Offset
     */
    private void LogMCB(int DS, int DX)
    {
    	addLine("MCB Base="+String.format("%04X", mms16.readMemoryWord((DS << 4) + DX))+" Length="+String.format("%04X", mms16.readMemoryWord((DS << 4) + DX + 2))+" Ext="+String.format("%02X", mms16.readMemoryByte((DS << 4) + DX + 4)));
    }

    /**
     * Aufruf von SCP Interrupt 224 loggen
     * @param DS Register DS
     * @param CL Register CL
     * @param DX Register DX
     */
	public void LogInt224(int DS, int CL, int DX) 
	{
		if (logFile == null) return;
		SCPFunction = CL;
		int DL = DX & 0xff;
    	switch (CL)
    	{
    	case 0:
    		addLine("Interrupt (0): SYSTEM RESET (Ret="+DL+")");
    		break;
    	case 1:
    		// Console Input: keine Parameter, LOG bei return
    		break;
    	case 2:
    		addLine("Interrupt (2): CONSOLE OUTPUT (Char=" + DL + (DL>30 ? " \""+(char)DL+"\"" : "")+")");
    		break;
    	case 3:
    		// AXI Input: keine Parameter, LOG bei return
    		break;
    	case 4:
    		addLine("Interrupt (4): PUNCH OUTPUT (Char=" + DL + (DL>30 ? " \""+(char)DL+"\"" : "")+")");
    		break;
    	case 5:
    		addLine("Interrupt (5): LIST OUTPUT (Char=" + DL + (DL>30 ? " \""+(char)DL+"\"" : "")+")");
    		break;
    	case 6:
    		if (DL<0xfe) 
    		{
    			addLine("Interrupt (6): CONSOLE DIRECT I/O (Output="+DL + (DL>30&&DL<0xfe ? " \""+(char)DL+"\"" : "")+")");
    		}
    		SCPParamDX = DX;
    		SCPParamDS = DS;
    		break;
    	case 7:
    		// Get IO Byte: keine Parameter, LOG bei return
    		break;
    	case 8:
    		addLine("Interrupt (8): SET I/O BYTE (Wert="+String.format("%08B", DL)+")");
    		break;
    	case 9:
    		String s = "";
    		for (int a=0; a<500; a++)
    		{
    			int b = mms16.readMemoryByte((DS << 4) + DX + a);
    			if (b>30)
    			{
    				if (b==36)
    				{
    					break;
    				}
    				s += (char)b;
    			}
    			else
    			{
    				switch (b)
    				{
    				case 13:
    					s += "\\r";
    					break;
    				case 10:
    					s += "\\n";
    					break;
    				case 0:
    					a=500;
    					break;
    				default:
        				s += "\\u"+String.format("%04X", b);
        				break;
    				}
    			}
    		}
    		addLine("Interrupt (9): PRINT STRING (String=\""+s+"\")");
    		break;
    	case 10:
    		addLine("Interrupt (10): READ CONSOLE BUFFER (Buffer="+String.format("0x%04X:0x%04X", DS, DX)+" Max="+mms16.readMemoryByte((DS << 4) + DX)+")");
    		SCPParamDX = DX;
    		SCPParamDS = DS;
    		break;
    	case 11:
    		// Get Console Status: keine Parameter, LOG bei return
    		break;
    	case 12:
    		// Get Version Nummer: keine Parameter, LOG bei return
    		break;
    	case 13:
    		addLine("Interrupt (13): RESET DISK SYSTEM");
    		break;
    	case 14:
    		addLine("Interrupt (14): SELECT DISK (Disk="+DL+")");
    		break;
    	case 15:
    		addLine("Interrupt (15): OPEN FILE (FCB="+String.format("0x%04X:0x%04X", DS, DX)+")");
    		SCPParamDS = DS;
    		SCPParamDX = DX;
    		LogFCB(DS, DX);
    		break;
    	case 16:
    		addLine("Interrupt (16): CLOSE FILE (FCB="+String.format("0x%04X:0x%04X", DS, DX)+")");
    		SCPParamDS = DS;
    		SCPParamDX = DX;
    		LogFCB(DS, DX);
    		break;
    	case 17:
    		addLine("Interrupt (17): SEARCH FOR FIRST (FCB="+String.format("0x%04X:0x%04X", DS, DX)+")");
    		SCPParamDS = DS;
    		SCPParamDX = DX;
    		LogFCB(DS, DX);
    		break;
    	case 18:
    		addLine("Interrupt (18): SEARCH FOR NEXT");
    		break;
    	case 19:
    		addLine("Interrupt (19):DELETE FILE (FCB="+String.format("0x%04X:0x%04X", DS, DX)+")");
    		SCPParamDS = DS;
    		SCPParamDX = DX;
    		LogFCB(DS, DX);
    		break;
    	case 20:
    		addLine("Interrupt (20): READ SEQUENTIAL (FCB="+String.format("0x%04X:0x%04X", DS, DX)+" DMA="+String.format("0x%04X:0x%04X", SCPDmaDS, SCPDmaDX)+")");
    		SCPParamDS = DS;
    		SCPParamDX = DX;
    		LogFCB(DS, DX);
    		break;
    	case 21:
    		addLine("Interrupt (21): WRITE SEQUENTIAL (FCB="+String.format("0x%04X:0x%04X", DS, DX)+" DMA="+String.format("0x%04X:0x%04X", SCPDmaDS, SCPDmaDX)+")");
    		SCPParamDS = DS;
    		SCPParamDX = DX;
    		LogFCB(DS, DX);
    		LogDmaBuffer(SCPDmaDS, SCPDmaDX);
    		break;
    	case 22:
    		addLine("Interrupt (22): MAKE FILE (FCB="+String.format("0x%04X:0x%04X", DS, DX)+")");
    		SCPParamDS = DS;
    		SCPParamDX = DX;
    		LogFCB(DS, DX);
    		break;
    	case 23:
    		addLine("Interrupt (23): RENAME FILE (FCB="+String.format("0x%04X:0x%04X", DS, DX));
        	s = "FCB Drive=" + mms16.readMemoryByte((DS << 4) + DX)+" Name(alt)=\"";
        	for (int a=0; a<8; a++) s += (char)mms16.readMemoryByte((DS << 4) + DX + 1 + a);
        	s += ".";
        	for (int a=0; a<3; a++) s += (char)mms16.readMemoryByte((DS << 4) + DX + 9 + a);
        	s += "\" Name(neu)=\"";
        	for (int a=0; a<8; a++) s += (char)mms16.readMemoryByte((DS << 4) + DX + 17 + a);
        	s += ".";
        	for (int a=0; a<3; a++) s += (char)mms16.readMemoryByte((DS << 4) + DX + 25 + a);
        	s += "\")";
        	addLine(s);
    		break;
    	case 24:
    		// RETURN LOGIN VECTOR: keine Parameter, LOG bei return
    		break;
    	case 25:
    		// RETURN CURRENT DISK: keine Parameter, LOG bei return
    		break;
    	case 26:
    		addLine("Interrupt (26): SET DMA ADRESS (Addr="+String.format("0x%04X", DX)+")");
    		SCPDmaDX = DX;
    		break;
    	case 27:
    		// GET ADDR (ALLOC): keine Parameter, LOG bei return
    		break;
    	case 28:
    		addLine("Interrupt (28): WRITE PROTECT DISK");
    		break;
    	case 29:
    		// GET READ ONLY VECTOR: keine Parameter, LOG bei return
    		break;
    	case 30:
    		addLine("Interrupt (30): SET FILE ATTRIBUTES (FCB="+String.format("0x%04X:0x%04X", DS, DX)+")");
    		SCPParamDS = DS;
    		SCPParamDX = DX;
    		LogFCB(DS, DX);
    		break;
    	case 31:
    		// GET ADDR (DISK PARMS): keine Parameter, LOG bei return
    		break;
    	case 32:
    		if (DL!=0xff)
    		{
        		addLine("Interrupt (32): SET USER CODE (User="+DL+")");
    		}
    		break;
    	case 33:
    		addLine("Interrupt (33): READ RANDOM (FCB="+String.format("0x%04X:0x%04X", DS, DX)+" DMA="+String.format("0x%04X:0x%04X", SCPDmaDS, SCPDmaDX)+")");
    		SCPParamDS = DS;
    		SCPParamDX = DX;
    		LogFCB(DS, DX);
    		break;
    	case 34:
    		addLine("Interrupt (34): WRITE RANDOM (FCB="+String.format("0x%04X:0x%04X", DS, DX)+" DMA="+String.format("0x%04X:0x%04X", SCPDmaDS, SCPDmaDX)+")");
    		SCPParamDS = DS;
    		SCPParamDX = DX;
    		LogFCB(DS, DX);
    		LogDmaBuffer(SCPDmaDS, SCPDmaDX);
    		break;
    	case 35:
    		addLine("Interrupt (35): COMPUTE FILE SIZE (FCB="+String.format("0x%04X:0x%04X", DS, DX)+")");
    		SCPParamDS = DS;
    		SCPParamDX = DX;
    		LogFCB(DS, DX);
    		break;
    	case 36:
    		addLine("Interrupt (36): SET RANDOM RECORD (FCB="+String.format("0x%04X:0x%04X", DS, DX)+")");
    		SCPParamDS = DS;
    		SCPParamDX = DX;
    		LogFCB(DS, DX);
    		break;
    	case 37:
    		addLine("Interrupt (37): RESET DISK (Vector="+String.format("0b%10B", DX)+")");
    		break;
    	case 40:
    		addLine("Interrupt (40): WRITE RANDOM WITH ZERO FILL (FCB="+String.format("0x%04X:0x%04X", DS, DX)+")");
    		SCPParamDS = DS;
    		SCPParamDX = DX;
    		LogFCB(DS, DX);
    		break;
    	case 47:
    		s = "Interrupt (47): CHAIN TO PROGRAM (Cmd=\"";
    		for (int a=0;a<128;a++)
    		{
    			int b = mms16.readMemoryByte((SCPDmaDS << 4) + SCPDmaDX + a);
    			if (b==0) break;
    			if (b>31)
    			{
    				s += (char)b;
    			}
    			else
    			{
    				s += "\\u"+String.format("%04X", b);
    			}
    		}
    		s += "\")";
    		addLine(s);
    		break;
    	case 50:
    		addLine("Interrupt (50): DIRECT BIOS CALL (BPB="+String.format("0x%04X:0x%04X", DS, DX)+" FN="+mms16.readMemoryByte((DS << 4) + DX)+" CX="+String.format("%04X", mms16.readMemoryWord((DS << 4) + DX + 1)) + " DX="+String.format("%04X", mms16.readMemoryWord((DS << 4) + DX + 3))+")");
    		break;
    	case 51:
    		addLine("Interrupt (51): SET DMA BASE (Seg="+String.format("0x%04X", DX)+")");
    		SCPDmaDS = DX;
    		break;
    	case 52:
    		// GET DMA BASE: keine Parameter, LOG bei return
    		break;
    	case 53:
    		addLine("Interrupt (53): GET MAX MEM (MCB="+String.format("0x%04X:0x%04X", DS, DX)+")");
    		SCPParamDS = DS;
    		SCPParamDX = DX;
    		LogMCB(DS, DX);
    		break;
    	case 54:
    		addLine("Interrupt (54): GET ABS MAX (MCB="+String.format("0x%04X:0x%04X", DS, DX)+")");
    		SCPParamDS = DS;
    		SCPParamDX = DX;
    		LogMCB(DS, DX);
    		break;
    	case 55:
    		addLine("Interrupt (55): ALLOC MEM (MCB="+String.format("0x%04X:0x%04X", DS, DX)+")");
    		SCPParamDS = DS;
    		SCPParamDX = DX;
    		LogMCB(DS, DX);
    		break;
    	case 56:
    		addLine("Interrupt (53): ALLOC ABS MEM (MCB="+String.format("0x%04X:0x%04X", DS, DX)+")");
    		SCPParamDS = DS;
    		SCPParamDX = DX;
    		LogMCB(DS, DX);
    		break;
    	case 57:
    		addLine("Interrupt (57): FREE MEM (MCB="+String.format("0x%04X:0x%04X", DS, DX)+")");
    		SCPParamDS = DS;
    		SCPParamDX = DX;
    		LogMCB(DS, DX);
    		break;
    	case 58:
    		addLine("Interrupt (58): FREE ALL MEM (MCB="+String.format("0x%04X:0x%04X", DS, DX)+")");
    		SCPParamDS = DS;
    		SCPParamDX = DX;
    		LogMCB(DS, DX);
    		break;
    	case 59:
    		addLine("Interrupt (59): PROGRAM LOAD (FCB="+String.format("0x%04X:0x%04X", DS, DX)+")");
    		SCPParamDS = DS;
    		SCPParamDX = DX;
    		LogFCB(DS, DX);
    		break;
    	default:
    		addLine("Interrupt: Unbekannte Function (CL="+CL+" DX="+String.format("0x%04X", DX)+")");
    		break;
    	}
		
	}

	
	/**
	 * IRET von Interrupt 224 loggen
	 * @param ES Register ES
	 * @param AX Register AX
	 * @param BX Register BX
	 */
	public void LogIRet224(int ES, int AX, int BX)
	{
		if (logFile == null) return;
		int AL = AX & 0xff;
		switch (SCPFunction)
		{
    	case 1:
    		addLine("Return (1): CONSOLE INPUT (Char=" + AL + (AL>30 ? " \""+(char)AL+"\"" : "")+")");
    		break;
    	case 3:
    		addLine("Return (3): READER INPUT (Char=" + AL + (AL>30 ? " \""+(char)AL+"\"" : "")+")");
    		break;
    	case 6:
    		if ((SCPParamDX & 0xff) == 0xfe)
    		{
        		addLine("Return (6): CONSOLE DIRECT I/O (Status=" + AL+")");
    		}
    		if ((SCPParamDX & 0xff) == 0xff && AL != 0)
    		{
        		addLine("Return (6): CONSOLE DIRECT I/O (Char=" + AL + (AL>30 ? " \""+(char)AL+"\"" : "")+")");
    		}
    		break;
    	case 7:
    		addLine("Return (7): GET I/O BYTE (Wert="+String.format("0b%08B", AL)+")");
    		break;
    	case 10:
    		String s = "";
    		for (int a=0;a<mms16.readMemoryByte((SCPParamDS << 4) + SCPParamDX + 1); a++)
    		{
    			s+=(char)mms16.readMemoryByte((SCPParamDS << 4) + SCPParamDX + 2 + a);
    		}
    		addLine("Return (10): READ CONSOLE BUFFER (String=\""+s+"\")");
    		break;
    	case 11:
    		addLine("Return (11): GET CONSOLE STATUS (Wert="+AL+")");
    		break;
    	case 12:
    		addLine("Return (12): RETURN VERSION NUMBER (Wert="+AL+")");
    		break;
    	case 15:
    		addLine("Return (15): OPEN FILE (RC="+AL+")");
    		LogFCB(SCPParamDS, SCPParamDX);
    		break;
    	case 16:
    		addLine("Return (16): CLOSE FILE (RC="+AL+")");
    		LogFCB(SCPParamDS, SCPParamDX);
    		break;
    	case 17:
    		addLine("Return (17): SEARCH FOR FIRST (RC="+AL+")");
    		LogFCB(SCPDmaDS, SCPDmaDX);
    		break;
    	case 18:
    		addLine("Return (18): SEARCH FOR NEXT (RC="+AL+")");
    		if (AL!=0xff)
    		{
    			LogFCB(SCPDmaDS, SCPDmaDX+AL*32);
    		}
    		break;
    	case 19:
    		addLine("Return (19): DELETE FILE (RC="+AL+")");
    		LogFCB(SCPParamDS, SCPParamDX);
    		break;
    	case 20:
    		addLine("Return (20): READ SEQUENTIAL (RC="+AL+")");
    		LogFCB(SCPParamDS, SCPParamDX);
    		LogDmaBuffer(SCPDmaDS, SCPDmaDX);
    		break;
    	case 21:
    		addLine("Return (21): WRITE SEQUENTIAL (RC="+AL+")");
    		LogFCB(SCPParamDS, SCPParamDX);
    		break;
    	case 22:
    		addLine("Return (22): MAKE FILE (RC="+AL+")");
    		LogFCB(SCPParamDS, SCPParamDX);
    		break;
    	case 23:
    		addLine("Return (23): RENAME FILE (RC="+AL+")");
    		break;
    	case 24:
    		addLine("Return (24): RETURN LOGIN VECTOR (Vector="+String.format("0b%10B", BX)+")");
    		break;
    	case 25:
    		addLine("Return (25): RETURN CURRENT DISK (Disk="+AL+")");
    		break;
    	case 27:
    		addLine("Return (27): GET ADDR (ALLOC) (Addr="+String.format("0x%04X:0x%04X", ES, BX)+")");
    		break;
    	case 29:
    		addLine("Return (29): GET READ ONLY VECTOR (Vector="+String.format("0b%10B", BX)+")");
    		break;
    	case 30:
    		addLine("Return (30): SET FILE ATTRIBUTES (RC="+AL+")");
    		LogFCB(SCPParamDS, SCPParamDX);
    		break;
    	case 31:
    		addLine("Return (31): GET ADDR (DISK PARMS) (DPB="+String.format("0x%04X:0x%04X", ES, BX)+")");
    		break;
    	case 32:
    		if ((SCPParamDX & 0xff) == 0xff)
    		{
    			addLine("Return (32): GET USER CODE (User="+AL+")");
    		}
    		break;
    	case 33:
    		addLine("Return (33): READ RANDOM (RC="+AL+")");
    		LogFCB(SCPParamDS, SCPParamDX);
    		LogDmaBuffer(SCPDmaDS, SCPDmaDX);
    		break;
    	case 34:
    		addLine("Return (34): WRITE RANDOM (RC="+AL+")");
    		LogFCB(SCPParamDS, SCPParamDX);
    		break;
    	case 35:
    		addLine("Return (35): COMPUTE FILE SIZE (RC="+AL+")");
    		LogFCB(SCPParamDS, SCPParamDX);
    		break;
    	case 36:
    		addLine("Return (36): SET RANDOM RECORD (RC="+AL+")");
    		LogFCB(SCPParamDS, SCPParamDX);
    		break;
    	case 40:
    		addLine("Return (40): WRITE RANDOM WITH ZERO FILL (RC="+AL+")");
    		LogFCB(SCPParamDS, SCPParamDX);
    		break;
    	case 52:
    		addLine("Return (52): GET DMA BASE (Seg="+String.format("0x%04X", BX)+")");
    		break;
    	case 53:
    		addLine("Return (53): GET MAX MEM (RC="+AL+")");
    		LogMCB(SCPParamDS, SCPParamDX);
    		break;
    	case 54:
    		addLine("Return (54): GET ABS MAX (RC="+AL+")");
    		LogMCB(SCPParamDS, SCPParamDX);
    		break;
    	case 55:
    		addLine("Return (55): ALLOC MEM (RC="+AL+")");
    		LogMCB(SCPParamDS, SCPParamDX);
    		break;
    	case 56:
    		addLine("Return (56): ALLOC ABS MEM (RC="+AL+")");
    		LogMCB(SCPParamDS, SCPParamDX);
    		break;
    	case 57:
    		addLine("Return (57): FREE MEM (RC="+AL+")");
    		LogMCB(SCPParamDS, SCPParamDX);
    		break;
    	case 58:
    		addLine("Return (58): FREE ALL MEM (RC="+AL+")");
    		LogMCB(SCPParamDS, SCPParamDX);
    		break;
    	case 59:
    		addLine("Return (59): PROGRAM LOAD (AX="+String.format("0x%04X", AX)+" BX="+String.format("0x%04X", BX)+")");
    		break;
		}
	}
}
